import React from "react";
import "./Register.scss";

const INITIAL_FORM = {
  firstName: "",
  lastName: "",
  age: "",
  email: "",
  image: "",
  education: [
    {
      title: "",
      company: "",
    },
  ],
  experience: [
    {
      title: "",
      company: "",
    },
  ],
  password: "",
  submitted: false,
};
class Register extends React.Component {

  constructor(props){
    super(props);
    this.state = INITIAL_FORM
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      submitted: true,
    })
    setTimeout(()=>{

      this.props.handleNewUser(this.state);
      this.setState(INITIAL_FORM);
    },2000);
  }
  
  onInputChange = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };
  render() {
    return (
      <form className="form" onSubmit={this.handleSubmit}>
        <h3>Register</h3>
        <label htmlFor="email">
          <p>Email :</p>
          <input
            type="email"
            name="email"
            value={this.state.email}
            placeholder="Email:"
            onChange={this.onInputChange}
            required
          />
        </label>
        <label htmlFor="password">
          <p>Password :</p>
          <input
            type="password"
            name="password"
            value={this.state.password}
            placeholder="Password:"
            onChange={this.onInputChange}
            required
          />
        </label>
        <button className="form__button" type="submit">
          Register
        </button>
        { this.state.submitted === true ?
         <p className="form__success" >Success!</p> : null}
      </form>
    );
  }
}

export default Register;


/*
** 
**
**
*/
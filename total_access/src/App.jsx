import React from "react";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
import Register from "./containers/Forms/Register";

import "./App.scss";

const HEADER_TITLE = "TOTAL ACCESS";
class App extends React.Component {

  constructor(props){
    super(props);
    this.state={
      register: false,
      userList: [],
    }
  }

  componentDidMount() {
    const users = JSON.parse(localStorage.getItem("users")) || [];
    this.setState({
      usersList: users
    });
  }

  handleNewUser = (user) => {
    const newUser = [...this.state.userList, user];

    this.setState({
        userList: [newUser],
      },
      () => {
        localStorage.setItem("userList", JSON.stringify(newUser));
      }
    );
    console.log(this.state.userList);
  };

  openMenu = (ev) => {
    const myClass = document.getElementById("nav-menu");
    if (myClass) {
      myClass.classList.toggle("sidebar-open");
    } else {
      myClass.classList.toggle("nav-menu");
    }
  };

  handleIconRegister = () => {
    this.setState({
      register: !this.state.register,
    });
  };

  togglePopup = () => {
    this.setState({
      showPopup: !this.state.showPopup,
    });
  };

  render() {
    const {register, users } = this.state;
    return ( 
      <div className = "container" >
      <header className = "header" >
      <Header TITLE = {HEADER_TITLE}/>
      <Navbar 
      register = {register}
      handleIconRegister = {this.handleIconRegister}
      openMenu = {this.openMenu}/>
      </header>
      <div className = "register" > 
      {register === true ? ( 
        <Register 
          users = {users}
          handleNewUser = {this.handleNewUser}
        />
        ) 
        : null
      }
      </div> 
      </div>
    );
  }
}

export default App;
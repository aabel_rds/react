import React from "react";
import PropTypes from "prop-types";
import "./Header.scss";

class Header extends React.Component {
  render() {
    return (
      <div className="header-container">
        <h1>{this.props.TITLE}</h1>
      </div>
    );
  }
}

Header.propTypes = {
  TITLE: PropTypes.string,
};
Header.defaultProps = {
  TITLE: "TOTAL ACCESS",
};
export default Header;

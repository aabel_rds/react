import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faIgloo,
  faIdBadge,
  faClipboard,
  faSignInAlt,
} from "@fortawesome/free-solid-svg-icons";
import "./Navbar.scss";

const navbarLinks = [faIgloo, faIdBadge, faClipboard, faSignInAlt];

class Navbar extends React.Component {
  register = () => {
    this.props.handleIconRegister();
  };

  render() {
    return (
      <nav className="nav">
        <div className="nav-burger">
          <input
            type="checkbox"
            className="menu-open"
            name="menu-open"
            id="menu-open"
            onChange={this.props.openMenu}
          />
          <label className="patty" htmlFor="menu-open">
            <span className="hamburger"></span>
          </label>
        </div>
        <div className="nav-menu" id="nav-menu">
          <ul className="nav-list">
            {navbarLinks.map((linkName) => {
              return (
                <li
                  key={`${linkName.prefix}__${linkName.iconName}`}
                  className="list-menu"
                >
                  <a
                    href="/#"
                    className="link-name"
                    onClick={linkName === faClipboard ? this.register : null}
                  >
                    <FontAwesomeIcon icon={linkName} />
                  </a>
                </li>
              );
            })}
          </ul>
        </div>
      </nav>
    );
  }
}

export default Navbar;

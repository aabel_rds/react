const passport = require("passport");
const User = require('../models/User');
const bcrypt = require('bcrypt');
const nodeMailer = require('nodemailer');


const saltRound = 10;
module.exports = {
    checkSession: async(req, res, next) => {
        if (req.user) {
            return res.status(200).json(req.user);
            
        } else {
            return res.status(401).json({ message: 'No user found' });
        }
    },

    registerPost: (req, res, next) => {
        console.log('Llega petición');
        passport.authenticate("register", (error, user) => {
            if (error) {
                return res.status(403).json({ message: error.message });
            }

            req.logIn(user, (error) => {
                if (error) {
                    return res.status(403).json({ message: error.message });
                }

                return res.json(user);
            });
        })(req, res, next);
    },

    loginPost: (req, res, next) => {
        passport.authenticate("login", (error, user) => {
            if (error) {
                return res.json({ message: error.message });
            }

            req.logIn(user, (error) => {
                if (error) {
                    return res.json({ message: error.message });
                }
                return res.json(user);
            });
        })(req, res, next);
    },

    logoutPost: (req, res, next) => {
        if (req.user) {
            req.logout();

            req.session.destroy(() => {
                res.clearCookie("connect.sid");

                return res.status(200).json({ message: 'Logout successful' });
            });
        } else {
            return res.status(401).json({ message: 'Unexpected error' });
        }
    },

    userGet: async (req, res, next) => {
        console.log('EN user', req.user);
        try {
            const user = req.user;

            return res.status(200).json(user);
        } catch (error) {
            next(error);
        }
    },

    editPut: async (req, res, next) => {
        const userId = req.user._id;
        const userImage = req.file ? req.file.filename : null;
        const userName = req.body.username ? req.body.username : req.user.username;
        const password = req.body.password ? req.body.password : req.user.password;
        const updatePassword = await bcrypt.hash(password, saltRound);
        
        try {
            const updateUser = await User.findByIdAndUpdate(
                userId,
                {
                username: userName,
                image: userImage,
                password: updatePassword
                },
                {new: true}
            );
            return res.status(200).json(updateUser); 
            
        } catch (error) {
            next(error);
        }
    },
    recoverPass: async (req, res, next) => {
        const email = req.body.email;
        const findUser = await User.find({email:email});

        try {

            if(!findUser){
                return res.status(500).json('El usuario no existe');
            }

            const characters = 
            '1 2 3 4 5 6 7 8 9 0 ! @ # $ % ^ & * ( ) _ - + = { } [ ] ; . , < > ? A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z';
            const finalCharacters = characters.split(' ');
            let password = '';

            const passwordGenerate = () => {
                
                for (let index = 0; index < 10 ; index++) {
                    password += finalCharacters[Math.floor(Math.random()* finalCharacters.length)];
                    
                }
                
                return password;
            }
            const generatedPass = passwordGenerate();
            const {_id} = findUser[0];
            const updatePassword = await bcrypt.hash(generatedPass, saltRound);
            const newPass = await User.findByIdAndUpdate(
                _id,
                {
                    password: updatePassword,
                },
                {new : true}

            );
            
            const transporter = nodeMailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'rithmeteam.maa@gmail.com',
                    pass: 'Upgrade.grupo5'
                }
            })
            const mailOptions = {
                from: 'rithmeteam.maa@gmail.com',
                to: 'aabelrds@gmail.com',
                subject: 'Enviado desde nodemailer',
                text: `Password provisional: ${generatedPass}`
    
            }
            transporter.sendMail(mailOptions, (error, info) =>{
                if(error){
                    console.log(error);
                }
                console.log('Enviado', info);
            });
            return res.status(200).json(newPass);
            

        } catch (error) {
            next(error);
        }


    }
};


const router = express.Router();

router.get('/', [isAuthenticated],usersController.userGet);
router.post('/register', usersController.registerPost);
router.post('/login' ,usersController.loginPost);
router.post('/logout',[isAuthenticated], usersController.logoutPost);
router.post('/recover-pass', usersController.recoverPass);
router.get('/check-session', usersController.checkSession);
router.put('/edit', [upload.single('image'), uploadToCloudinary], usersController.editPut);


module.exports = router;